# Project Name: Ansible Deployment
## Overview
Ansible project that can deploy a Spring Boot application to a server. The project is designed to be flexible and easily customizable to fit a variety of use cases. It is also designed to be secure, with sensitive information stored in an encrypted vault file. MySQL (database) and Nginx (reverse proxy and load balancer) are also installed and configured as part of the deployment process.

## Prerequisites
* Ansible
* Python
* Pip
* PyMySQL (Python MySQL client library, only for running tests)
* community.mysql (Ansible MySQL client library, only for running tests)

## Structure
Overview of the project directory structure.

ansible_project/

├── playbooks/

│   └── spring_app_setup.yml <-- Main playbook

├── roles/

│   ├── spring_app/

│   ├── nginx/

│   ├── mysql/

│   └── ...

├── inventory/

│   └── hosts.ini

├── tests/ <-- Playbooks running tests for the project

├── ansible.cfg

└── vault.yml

## Roles
### Spring App
A simple Java 17 Spring Boot application that serves as a forum. Source: https://gitlab.com/IZOBRETATEL777/hacker-suitcases
The artifact is built using by the CI/CD pipeline of the web-site project and docker file is available in [the registry](https://gitlab.com/IZOBRETATEL777/hacker-suitcases). The artifact is then copied to the remote server and run as a systemd service.

### Nginx
NGinx is installed and configured as a reverse proxy and load balancer for the Spring Boot application.

### MySQL
MySQL is installed and configured as the database for the Spring Boot application.

### (Other Roles)
By default Docker will be installed on all hosts. Additionally, a serrated Docker network (SDN) will be created to incapsulate the Spring Boot application and MySQL database. The SDN will be used to connect the Spring Boot application and MySQL database containers.

## Main Playbook
The ide was to create a single playbook with multiple roles.

Usage:

```sh
ansible-playbook playbooks/spring_app_setup.yml --ask-vault-pass --ask-become-pass
```

During the execution you will be asked to enter the vault password (`--ask-vault-pass`) and the sudo password (`--ask-become-pass`).

The result of the playbook execution is a running Spring Boot application with a MySQL database and Nginx load balancer.The Spring Boot application is accessible at `http://localhost`.

## Inventory
Description of the inventory structure, explaining hosts.ini and any group-specific configurations.

Hosts File (inventory/hosts.ini)

```ini
[webserver] - server that hosts the Spring Boot application
localhost ansible_connection=local ansible_python_interpreter=/usr/bin/python3

[databaseserver] - server that hosts the MySQL database
localhost ansible_connection=local ansible_python_interpreter=/usr/bin/python3

[lbserver] - server that hosts the Nginx load balancer
localhost ansible_connection=local ansible_python_interpreter=/usr/bin/python3
```

By default, the inventory file is configured to run the playbook on the local machine. To run the playbook on a remote server, change the localhost value to the IP address of the remote server and corresponding SSH credentials.

## Variables
Relevant variables and their default values are stored in `roles/<role-name>/main.yaml`. Some of them are sensative and are stored in the vault file.

To access the vault file, run the following command:

```sh
ansible-vault edit vault.yml
```

You will be asked to enter the vault password.


```sh
ansible-playbook tests/testing_playbook --ask-vault-pass --ask-become-pass
```

## Testing

There are two test phases:
* Database Server Test (tests/db_server_test.yml) - tests the MySQL database for
    1. Availability
    2. Possibility to create a table, insert and select data from it.
* Web Server Test (tests/spring_app_test.yml)

The tests are run using the following command:

```sh
ansible-playbook tests/<testing_playbook> --ask-vault-pass --ask-become-pass
```
See the prerequisites section for the required Python libraries.